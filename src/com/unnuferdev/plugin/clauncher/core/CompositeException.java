package com.unnuferdev.plugin.clauncher.core;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.osgi.util.NLS;

import com.unnuferdev.plugin.clauncher.Activator;

public class CompositeException {
	public static CoreException build(String messageFormat, Object... args) {
		return new CoreException((IStatus) new Status(IStatus.ERROR, Activator.PLUGIN_ID, NLS.bind(messageFormat, args)));
	}
}
