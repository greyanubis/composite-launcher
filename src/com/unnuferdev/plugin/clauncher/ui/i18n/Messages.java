package com.unnuferdev.plugin.clauncher.ui.i18n;

import org.eclipse.osgi.util.NLS;

public class Messages {
	
	private static final String MESSAGES = "com.unnuferdev.plugin.clauncher.ui.i18n.messages"; 
	
	public static String TAB_NAME;
	public static String UP;
	public static String ADD;
	public static String REMOVE;
	public static String DOWN;
	public static String SELECTION_DIALOG_TITLE;
	
	static {
		NLS.initializeMessages(MESSAGES, Messages.class);
	}
	
}
