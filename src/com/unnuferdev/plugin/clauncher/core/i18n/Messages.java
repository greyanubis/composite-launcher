package com.unnuferdev.plugin.clauncher.core.i18n;

import org.eclipse.osgi.util.NLS;

public class Messages {
	
	private static final String MESSAGES = "com.unnuferdev.plugin.clauncher.core.i18n.messages"; 
	
	public static String INVALID_COMPOSITE_CONFIGURATION_MESSAGE_FORMAT;
	public static String UNSUPPORTED_LAUNCH_MODE_MESSAGE_FORMAT;
	
	static {
		NLS.initializeMessages(MESSAGES, Messages.class);
	}
	
}
