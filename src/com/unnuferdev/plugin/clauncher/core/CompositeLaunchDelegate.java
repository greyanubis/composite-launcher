package com.unnuferdev.plugin.clauncher.core;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.IDebugTarget;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;

import com.unnuferdev.plugin.clauncher.core.i18n.Messages;
import com.unnuferdev.plugin.clauncher.model.ConfigurationHelper;

public class CompositeLaunchDelegate extends LaunchConfigurationDelegate  {

	@Override
	public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
			throws CoreException {

		final List<ILaunchConfiguration> childConfigurations = ConfigurationHelper.getChildConfigurations(configuration);
		final SubMonitor subMonitor = SubMonitor.convert(monitor, configuration.getName(), childConfigurations.size());

		if (!ConfigurationHelper.isValidConfiguration(configuration)) {
			throw CompositeException.build(Messages.INVALID_COMPOSITE_CONFIGURATION_MESSAGE_FORMAT, configuration.getName());
			
		}
		if (!ConfigurationHelper.isSupportedMode(mode)) {
			throw CompositeException.build(Messages.UNSUPPORTED_LAUNCH_MODE_MESSAGE_FORMAT, mode);
		}

		try {
			for (ILaunchConfiguration child: childConfigurations) {
				if (!monitor.isCanceled()) {
					final ILaunch childLaunch = child.launch(mode, subMonitor.newChild(1));

					for (IDebugTarget target : childLaunch.getDebugTargets()) {
						launch.addDebugTarget(target);
					}

					for (IProcess process : childLaunch.getProcesses()) {
						launch.addProcess(process);
					}
				}
			}
		} finally {
			monitor.done();
		}
	}
}
