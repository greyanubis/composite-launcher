package com.unnuferdev.plugin.clauncher.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

public class CompositeManager {
	private ILaunchConfiguration configuration;
	private List<ILaunchConfiguration> childConfigurations;
	
	public enum Move { UP, DOWN };
	
	public CompositeManager(ILaunchConfiguration configuration) {
		this.configuration = configuration;
		this.childConfigurations = ConfigurationHelper.getChildConfigurations(configuration);
	}

	public ILaunchConfiguration getConfiguration() {
		return configuration;
	}
	
	public List<ILaunchConfiguration> getChildConfigurations() {
		return childConfigurations;
	}

	public ILaunchConfiguration getChildConfiguration(int index) {
		return childConfigurations.get(index);
	}

	public void addChildConfiguration(ILaunchConfiguration childConfiguration) {
		if (notTheSameConfiguration(childConfiguration)) {
			childConfigurations.add(childConfiguration);
		}
	}

	public void addChildConfigurations(Collection<? extends ILaunchConfiguration> childConfigurations) {
		for (ILaunchConfiguration childConfiguration: childConfigurations) {
			this.addChildConfiguration(childConfiguration);
		}
	}

	public void removeChildConfiguration(int index) {
		getChildConfigurations().remove(index);
	}

	public void removeChildConfigurations(int[] indices) {
		for (int index: indices) {
			removeChildConfiguration(index);
		}
	}

	public void removeChildConfigurations(Collection<? extends ILaunchConfiguration> childConfigurations) {
		getChildConfigurations().removeAll(childConfigurations);
	}

	public int moveChildConfiguration(Move move, ILaunchConfiguration child) {
		int currentIndex = getChildConfigurations().indexOf(child);
		int newIndex = currentIndex;
		if (currentIndex != -1) {
			newIndex = (move == Move.UP) ? 
				 ((currentIndex-1) >= 0 ? currentIndex-1 : currentIndex) : 
				 ((currentIndex+1) < getChildConfigurations().size() ? currentIndex+1 : currentIndex);

			Collections.swap(getChildConfigurations(), currentIndex, newIndex);
		}
		return newIndex;
	}

	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		ConfigurationHelper.setChildConfigurations(configuration, childConfigurations);
	}
	
	public List<ILaunchConfiguration> checkChildrenForLoops() {
		List<ILaunchConfiguration> configurationsWithLoops = new ArrayList<>();
		
		for (ILaunchConfiguration child: getChildConfigurations()) {
			if (ConfigurationHelper.doContain(child, this.getConfiguration())) {
				configurationsWithLoops.add(child);
			}
		}
		return configurationsWithLoops;
	}
	
	private boolean notTheSameConfiguration(ILaunchConfiguration configuration) {
		return !this.configuration.getName().equals(configuration.getName());
	}
}
