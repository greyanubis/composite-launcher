package com.unnuferdev.plugin.clauncher.ui;

public class LaunchConfigurationNameColumn extends LaunchConfigurationColumn {
	@Override
	public int getWidth() {
		return 500;
	}

	@Override
	public String getText(Object element) {
		return element.toString();
	}

	@Override
	public String getTitle() {
		return "Name";
	}
}
