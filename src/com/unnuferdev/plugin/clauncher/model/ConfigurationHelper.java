package com.unnuferdev.plugin.clauncher.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;

public class ConfigurationHelper {
	public static final String COMPOSITE_LAUNCH_CONFIGURATION_TYPE = "com.unnuferdev.plugin.clauncher.core.launchConfigurationType";
	public static final String ATTRIBUTE_COMPOSITE_LAUNCH_CHILDREN = "ATTRIBUTE_COMPOSITE_LAUNCH_CHILDREN";
	
	public static boolean isCompisiteLaunchConfiguration(ILaunchConfiguration configuration) {
		try {
			return configuration.getType().getIdentifier().equals(COMPOSITE_LAUNCH_CONFIGURATION_TYPE);
		} catch (CoreException e) {
			return false;
		}
	}
	
	public static List<ILaunchConfiguration> getChildConfigurations(ILaunchConfiguration configuration) {
		Map<String, ILaunchConfiguration> configurationMap = getLaunchConfigurationsMap();
		List<ILaunchConfiguration> childConfigurations = new ArrayList<>();
		
		try {
			List<String> configNames = 
					configuration.getAttribute(ATTRIBUTE_COMPOSITE_LAUNCH_CHILDREN,
							                   new ArrayList<String>());
			for (String name: configNames) {
				if (configurationMap.containsKey(name)) {
					childConfigurations.add(configurationMap.get(name));
				}
			}
		} catch (CoreException e) {
			e.printStackTrace();
		}

		return childConfigurations;
	}
	
	public static void setChildConfigurations(ILaunchConfigurationWorkingCopy configuration, final List<ILaunchConfiguration> childConfigurations) {
		configuration.setAttribute(ATTRIBUTE_COMPOSITE_LAUNCH_CHILDREN, 
								   convertConfigurationsToNames(childConfigurations));;
	}
	
	public static boolean doContain(ILaunchConfiguration parent, ILaunchConfiguration child) {
		boolean loopDetected = false;
		Stack<ILaunchConfiguration> stack = new Stack<>();
		stack.add(parent);
		
		while (stack.isEmpty() == false && 
			   loopDetected == false) {
			for (ILaunchConfiguration configuration: getChildConfigurations(stack.pop())) {
				if (isCompisiteLaunchConfiguration(configuration)) {
					if (configuration.getName().equals(child.getName())) {
						loopDetected = true;
						break;
					}
					stack.push(configuration);
				}
			}
		}
		return loopDetected;
	}
	
	public static boolean hasLoop(ILaunchConfiguration configurations) {
		boolean loopDetected = false;
		if (isCompisiteLaunchConfiguration(configurations)) {
			Queue<ILaunchConfiguration> queue = new LinkedList<>();
			queue.add(configurations);

			while (queue.isEmpty() == false && 
				   loopDetected == false) {
				ILaunchConfiguration checkedConfig = queue.poll();
				for (ILaunchConfiguration child: getChildConfigurations(checkedConfig)) {
					if (isCompisiteLaunchConfiguration(child)) {
						if (child.getName().equals(checkedConfig.getName())) {
							loopDetected = true;
							break;
						}
						if (doContain(child, checkedConfig)) {
							loopDetected = true;
							break;
						}
						queue.add(child);
					}
				}
			}
		}
		return loopDetected;
	}

	public static List<ILaunchConfiguration> getAllLaunchConfigurations()  {
		try {
			return new ArrayList<ILaunchConfiguration>(
				Arrays.asList(DebugPlugin.getDefault().getLaunchManager().getLaunchConfigurations()));
		} catch (CoreException e) {
			return new ArrayList<ILaunchConfiguration>();
		}
	}

	public static List<ILaunchConfiguration> getAllLaunchConfigurations(ILaunchConfiguration excluding)  {
		List<ILaunchConfiguration> configurations = getAllLaunchConfigurations();
		Iterator<ILaunchConfiguration> iterator = configurations.iterator();
		
		while (iterator.hasNext()) {
			if (iterator.next().getName().equals(excluding.getName())) {
				iterator.remove();
				break;
			}
		}
		return configurations;
	}

	public static Map<String, ILaunchConfiguration> getLaunchConfigurationsMap()  {
		Map<String, ILaunchConfiguration> map = new HashMap<String, ILaunchConfiguration>();
		for (ILaunchConfiguration config: getAllLaunchConfigurations()) {
			map.put(config.getName(), config);
		}
		return map;
	}

	public static List<String> convertConfigurationsToNames(Collection<ILaunchConfiguration> configurations) {
		List<String> list = new ArrayList<String>();
		for (ILaunchConfiguration configuration: configurations) {
			list.add(configuration.getName());
		}
		return list;
	}

	public static List<String> convertConfigurationsToExtendedNames(Collection<ILaunchConfiguration> configurations) {
		List<String> list = new ArrayList<String>();
		for (ILaunchConfiguration configuration: configurations) {
			list.add(ConfigurationHelper.getConfigurationExtendedName(configuration));
		}
		return list;
	}

	public static List<ILaunchConfigurationType> getAllLaunchConfigurationTypes() {
		List<ILaunchConfigurationType> types = new ArrayList<>();
		return types;
	}

	public static String getConfigurationExtendedName(ILaunchConfiguration configuration) {
		try {
			return new StringBuffer()
				.append(configuration.getType().getName())
				.append("::")
				.append(configuration.getName())
				.toString();
		} catch (CoreException e) {
			return null;
		}
	}

	public static boolean isSupportedMode(String mode) {
		return DebugPlugin.getDefault().getLaunchManager().RUN_MODE.equalsIgnoreCase(mode) ||
			   DebugPlugin.getDefault().getLaunchManager().DEBUG_MODE.equalsIgnoreCase(mode);
		
	}

	public static boolean isValidConfiguration(ILaunchConfiguration configuration) {
		return isCompisiteLaunchConfiguration(configuration);
	}
}
