package com.unnuferdev.plugin.clauncher.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;

import com.unnuferdev.plugin.clauncher.model.CompositeManager;
import com.unnuferdev.plugin.clauncher.model.CompositeManager.Move;
import com.unnuferdev.plugin.clauncher.model.ConfigurationHelper;
import com.unnuferdev.plugin.clauncher.ui.i18n.Messages;

public class CompositeLaunchConfigurationTab extends AbstractLaunchConfigurationTab  {

	@Override
	public void createControl(Composite parent) {
		final Composite container = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout(2, true);
		gridLayout.makeColumnsEqualWidth = false;

		container.setLayout(gridLayout);
	    setControl(container);

		setupLaunchesTable(container);
		setupControlButtons(container);

		setFocus();
	}

	@Override
	public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
	}

	@Override
	public void initializeFrom(ILaunchConfiguration configuration) {
		compositeManager = new CompositeManager(configuration);
		fillTable(compositeManager.getChildConfigurations());
	}

	@Override
	public void performApply(ILaunchConfigurationWorkingCopy configuration) {
		compositeManager.performApply(configuration);
	}

	@Override
	public String getName() {
		return Messages.TAB_NAME;
	}

	private CompositeManager compositeManager;
	private TableViewer tableViewer;

	private void setupLaunchesTable(Composite parent) {
		GridData gridData = new GridData();
		gridData.verticalSpan = 4;

		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;

		gridData.verticalAlignment = GridData.FILL;
		gridData.grabExcessVerticalSpace = true;


		tableViewer = new TableViewer(parent, SWT.V_SCROLL|SWT.H_SCROLL);
		tableViewer.getTable().setHeaderVisible(true);
		tableViewer.getTable().setLayoutData(gridData);
		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		
		new LaunchConfigurationNameColumn().addColumnTo(tableViewer);
	}

	private void setFocus() {
		tableViewer.getControl().setFocus();
	}
	
	private Button createControlButton(Composite parent, String label) {
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setLayoutData(new GridData(SWT.FILL, SWT.NONE, false, false));
		return button;
	}
	
	private void setupControlButtons(Composite parent) {
		createControlButton(parent, Messages.UP).addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				handleLaunchConfigurationMove(Move.UP);
			}
		});

		createControlButton(parent, Messages.DOWN).addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				handleLaunchConfigurationMove(Move.DOWN);
			}
		});

		createControlButton(parent, Messages.ADD).addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				handleLaunchConfigurationAdd();
			}
		});

		createControlButton(parent, Messages.REMOVE).addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				handleLaunchConfigurationRemove();
			}
		});
	}

	private void fillTable(Collection<? extends ILaunchConfiguration> configuratins) {
		tableViewer.getTable().removeAll();
		for (ILaunchConfiguration configuration: configuratins) {
			tableViewer.add(ConfigurationHelper.getConfigurationExtendedName(configuration));
		}
	}

	private void showLoopWarningDialog(List<ILaunchConfiguration> configurationsWithLoop) {
		StringBuffer message = new StringBuffer();
		message.append("Following")
			.append(" configuration")
			.append(configurationsWithLoop.size() > 1 ? "s" : "");

		String separator = " ";
		for (ILaunchConfiguration configuration: configurationsWithLoop) {
			message.append(separator)
			   	   .append(configuration.getName());
			separator = ", "; 
		}

		message.append(" contain")
			   .append(configurationsWithLoop.size() > 1 ? "s" : "")
			   .append(" loop. \n")
			   .append("Ignore ")
			   .append(configurationsWithLoop.size() > 1 ? "them" : "it");

		MessageBox messageDialog = new MessageBox(getShell(), SWT.ERROR);
	    messageDialog.setText("Loop detected");
	    messageDialog.setMessage(message.toString());
	    messageDialog.open();
	}
	
	private void handleLaunchConfigurationAdd() {
		List<ILaunchConfiguration> allConfigurations = 
				ConfigurationHelper.getAllLaunchConfigurations(compositeManager.getConfiguration());
		allConfigurations.sort(new Comparator<ILaunchConfiguration>() {
			@Override
			public int compare(ILaunchConfiguration o1, ILaunchConfiguration o2) {
				if (o1 == o2)   return 0;
				if (o1 == null) return -1;
				if (o2 == null) return 1;

				try {
					return o1.getType().getName().compareTo(o2.getType().getName());
				} catch (CoreException e) {
					return -1;
				}
			}
		});
				
		List<String> configurationsExtendedNames = 
				ConfigurationHelper.convertConfigurationsToExtendedNames(allConfigurations);

		final ListSelectionDialog<String> dialog =
				new ListSelectionDialog<>(getShell(), configurationsExtendedNames);

		if (dialog.open() == Window.OK) {
			int[] selectedIndices = dialog.getSelectedIndices();
			
			final List<ILaunchConfiguration> selectedConfiguratins = new ArrayList<>();
			for (int index: selectedIndices) {
				selectedConfiguratins.add(allConfigurations.get(index));
			}

			compositeManager.addChildConfigurations(selectedConfiguratins);

			final List<ILaunchConfiguration> configurationsWithLoop = 
					compositeManager.checkChildrenForLoops();
			if (configurationsWithLoop.size() > 0) {
				compositeManager.removeChildConfigurations(configurationsWithLoop);
				showLoopWarningDialog(configurationsWithLoop);
			}

			updateLaunchConfigurationDialog();
			fillTable(compositeManager.getChildConfigurations());
		}
	}
	
	private void handleLaunchConfigurationRemove() {
		if (tableViewer.getTable().getSelectionIndices().length > 0) {
			int[] selectedIndices = tableViewer.getTable().getSelectionIndices();

			tableViewer.getTable().remove(selectedIndices);
			compositeManager.removeChildConfigurations(selectedIndices);

			updateLaunchConfigurationDialog();
		}
	}

	private void handleLaunchConfigurationMove(Move move) {
		if (tableViewer.getTable().getSelectionIndices().length > 0) {
			int selectedIndex = tableViewer.getTable().getSelectionIndex();

			int newIndex = compositeManager.moveChildConfiguration(move, 
					compositeManager.getChildConfiguration(selectedIndex));

			fillTable(compositeManager.getChildConfigurations());

			setFocus();
			tableViewer.getTable().setSelection(newIndex);

			updateLaunchConfigurationDialog();
		}
	}
}
