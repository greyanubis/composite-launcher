package com.unnuferdev.plugin.clauncher.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import com.unnuferdev.plugin.clauncher.ui.i18n.Messages;

public class ListSelectionDialog<T> extends TitleAreaDialog {
	private List list;

	private java.util.List<T> items;
	private java.util.List<T> selectedItems = new ArrayList<T>();
	private int[] selectedIndices = new int[0];
	private java.util.Map<Integer, T> map = new HashMap<Integer, T>();

	public ListSelectionDialog(Shell parentShell, Collection<T> items) {
		super(parentShell);
		this.items = new ArrayList<>(items);
	}
	
	@Override
	public void create() {
		super.create();
		setTitle(Messages.SELECTION_DIALOG_TITLE);
	}
	
	public java.util.List<T> getSelectedItems() {
		return selectedItems;
	}

	public int[] getSelectedIndices() {
		return selectedIndices;
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
	    Composite area = (Composite) super.createDialogArea(parent);
	    Composite container = new Composite(area, SWT.NONE);
	    container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	    GridLayout layout = new GridLayout(2, false);
	    container.setLayout(layout);
	    
	    list = new List(container, SWT.MULTI | SWT.BORDER | SWT.H_SCROLL);
	    configListView(list);
	    fillList(list);

	    return area;
	}
	
	@Override
	protected void okPressed() {
		getSelectionItems();
		super.okPressed();
	}
	
	private void configListView(List list) {
		GridData gridData = new GridData();
		gridData.horizontalAlignment = GridData.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.verticalSpan = 4;
		gridData.verticalAlignment = GridData.BEGINNING;
		
		list.setLayoutData(gridData);
	}
	
	private void fillList(List list) {
		if (items != null) {
			int index = 0;

			for (T item: items) {
				list.add(item.toString());
				map.put(index++, item);
			}
		}
	}
	
	private void getSelectionItems() {
		selectedIndices = list.getSelectionIndices();

		for (int i: list.getSelectionIndices()) {
			selectedItems.add(map.get(i));
		}
	}
}
